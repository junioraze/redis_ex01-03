import redis
import operator
fmt = "-"*20

def redis_conn(host, port, password,decode_response=True):

	try:
		r_conn = redis.StrictRedis(host=host, port=port, password=password,
			decode_responses=decode_response)

		return r_conn

	except Exception as e:
		print("Connection failed:\n{}".format(e))

		return 0

def print_all(r):

	r_keys = r.keys()
	r_length = len(r_keys)

	print("Printing all keys...")
	print("Number of Keys in RedisDB -> {}".format(r_length))

	for key in r_keys:
		r_type = r.type(key.encode("utf-8"))
		if r_type == "string":
			print("\tkey: {}| type: {}| value: {}: ".format(key,r_type,r.get(key)))
		elif r_type == "list":
			print("\tkey: {}| type: {}| value: {}".format(key,r_type,slugfy(r.lrange(key,0,-1))))
		elif r_type == "hash":
			print("\tkey: {}| type: {}| value: {}".format(key,r_type,r.hgetall(key)))
	

def flush_all(r):

	r.flushall()
	print("Flushall executed...")

def slugfy(r_lista):

	lista = [value.encode("utf-8") for value in r_lista[0][1:][:-1].split(", ")]
	return lista

def make_comment(r, key):
	like_types = ['like','love','haha','wow','sad','angry']
	for item in like_types:
		r.zadd(key, 0, item)
	print("{} comment inserted!".format(key))

def make_like(r, *likes):

	try:
		for like in likes:
			r.zincrby(like[0],like[1],1)
			print("Comment {} receive a {}!".format(like[0], like[1]))

	except Exception as e:
		print("{} isn't a valid comment!\n{}".format(like[0],e))

def make_rank(r, comment, *types):

	rank = {}
	for t in types:
		rank[t] = r.zrank(comment,t)
	return rank


def ex_01(r):

	#insert keys(a, b, )
	r.set("a",30)
	r.set("b",33)
	r.set("c","Comentario")

	#increment a
	r.incr("a")
	r.incrby("b",10)

	print(fmt+" EX_01 "+fmt)
	print_all(r)
	flush_all(r)

def ex_02(r):

	print(fmt+" EX_02 "+fmt)

	#insert list [1..15]
	r.rpush("list:1:15",range(1,16))

	#make hash atributes dicts
	user_1_dict = {
			"nome":"Vanessa",
			"idade":23
		}

	user_2_dict = {
			"nome":"Katia",
			"idade":22
		}

	#set hashs
	r.hmset("user:1", user_1_dict)
	r.hmset("user:2", user_2_dict)
	print_all(r)

	#result in hardcoding
	print(fmt+" EX_02_RESULTS "+fmt)
	print("list_result: {}\nvanessa_age: {}".format(slugfy(r.lrange("list:1:15",0,-1)),r.hget("user:1","idade"))) 
	flush_all(r)

def ex_03(r):

	print(fmt+" EX_03 "+fmt)
	comment = "facebook:comment:1"
	make_comment(r, comment)
	make_like(r, (comment, 'like'),(comment, 'love'),(comment, 'love'),(comment, 'wow'),(comment, 'love'),
			(comment, 'like'),(comment, 'like'),(comment, 'wow'))

	print(fmt+" LIKE RANKING "+fmt)
	like_ranking = make_rank(r, comment, 'like','love','haha','wow','sad','angry')
	sorted_order = sorted(like_ranking.items(), key=operator.itemgetter(1), reverse=True)
	
	for i in range(2): #se trocar range(2) por sorted_order tem o ranking geral
		l_type = sorted_order[i][0]
		amount = r.zscore(comment, l_type)
		print("Rank {} -> Type: {} Amount: {}".format((i+1), l_type ,amount))
	
	flush_all(r)
	
if __name__ == "__main__":

	r_conn = redis_conn("localhost", 6379, "")

	if r_conn == 0:
		print("Exiting...")
	else:
		ex_01(r_conn)
		ex_02(r_conn)
		ex_03(r_conn)
	
